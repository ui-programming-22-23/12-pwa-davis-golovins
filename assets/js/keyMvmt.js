
// initialises
let adiv = document.getElementById('mydiv'); // initialise mydiv
var canvas = document.getElementById('the_canvas'),  //initialise canvas
    context = canvas.getContext('2d');


// initialise player
let image = new Image();
image.src = "assets/img/ss.png";
let speed = 3;


// initialise wall obstacle
let obstacle = new Image();
obstacle.src = "assets/img/castle.png";

// initialise score counter 
let score = 0; 

// initialise data from local storage
let savedScore = localStorage.getItem('score');
let savedName = localStorage.getItem('name');


// initialise health potion
let healthpickup = new Image();
healthpickup.src = "assets/img/HPPOT.png";


// variables set for healtbar
var width = 100;
var height = 20;
var max = 100;
let val = 10;


// initialise hppot pickup
let pickup = false;

let direction = 0;

// Timer for holding E timeout.
let timer = 0;

// Initialise Animation
animationtimer = 0;
frame = 0;
var row = 0;
var column = 0;
// collision for castle
let collided = {
    bottom: false,
    right: false,
    left: false,
    top: false
};

  // GameObject holds positional information
  // Can be used to hold other information based on requirements
  function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}
  
// Default Player
let player = new GameObject(image, 137,137, 102, 102)

// Default castle
let castle = new GameObject(obstacle, 500, 200, 400, 100);

// Default healthpot
let healthpot = new GameObject(healthpickup , 400 , 300 ,100, 100);

  
// Default buttons 
let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];

// start of functions
function GamerInput(input) {
this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

window.addEventListener('keydown', inputHandle);  // key listener so that we can have an event happen when the keydown event is triggered   

function inputHandle(event){
    if(event.type === "keydown"){
        switch(event.keyCode){
            case 37:
                gamerInput = new GamerInput("Left");
                direction = 2;
                break;
            case 38:
                gamerInput = new GamerInput("Up");
                direction = 4;
                break;
            case 39:
                gamerInput = new GamerInput("Right");
                direction = 3;
                break; 
            case 40:
                gamerInput = new GamerInput("Down");
                direction = 1;
                break; 
            case 81:  
                gamerInput = new GamerInput("q");   // increases player size while held down
                break;
            case 87:
                gamerInput = new GamerInput("w");   //  decreases player size while held down
                break; 
            case 69:
                gamerInput = new GamerInput("e");  // if e key held down changes player color to a random rgb value  
                break;    
            default:
            console.log("default state");
        }
    }
    else{
        gamerInput = new GamerInput("None");
    }
}

window.addEventListener('keyup', inputHandle);  // event listener for releasing a key
window.addEventListener('keydown', inputHandle);


// Timer Top Right of screen
function startTime() {
    const today = new Date();    // Date() function assigns it to today
    // assign hours , minutes , seconds
    let hours = today.getHours();
    let minutes = today.getMinutes();
    let seconds = today.getSeconds();
    minutes = checkTime(minutes);
    seconds = checkTime(seconds);
    document.getElementById('mydiv').innerHTML =  hours + ":" + minutes + ":" + seconds;
    setTimeout(startTime, 1000);
  }
  
  //check for 0
  function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10, 01 02 etc.
    return i;
  }
  startTime();

  //get the form element
  const scoreform = document.querySelector("retrieveform")
  const form = document.querySelector('myform');

  // add event listener for formsubmission
  myform.addEventListener('submit', function (event) {
      // prevent blank form submission
      event.preventDefault();
      console.log("help");
      // get the player name input element
      const playerNameInput = document.querySelector('#playerName');
      // get the player name value
      const playerName = playerNameInput.value;
      // save the player name to local storage 
      localStorage.setItem('playerName',playerName);
      // display the player name on screen 
      const playerNameDisplay = document.createElement('p');
      playerNameDisplay.textContent = 'Welcome ' + playerName + "!";
      document.body.appendChild(playerNameDisplay);
      // clear the input field
      playerNameInput.value = '';
      // retrieve the score or not 
      retrieveScore();

      document.getElementById("mydiv").style.visibility = "visible";
      document.getElementById("the_canvas").style.display = "block";
      document.getElementById("myform").style.display = "none";
      document.getElementById("nameInput").style.display ="none";
      document.getElementById("retrieveform").style.display = "none";
      
  }); 

function retrieveScore(){

  var scoreOption = document.querySelector('input[name="scoreOption"]:checked').value;
  if (scoreOption === "retrieve") {
    // Retrieve the score from local storage and use it
    score = localStorage.getItem("savedscore");
    console.log("Retrieved score:", score);
  } else {
    // Start with a fresh score
    score = 0;
    console.log("Starting with a fresh score");
  }
}




// Castle obstacle collision function
function collision() {
    if (player.x + player.width >= castle.x && player.x <= castle.x + castle.width &&
        player.y + player.height >= castle.y && player.y <= castle.y + castle.height)
    { 
      
      const top_diff = castle.y + castle.height - player.y;
      const bottom_diff = player.y + player.height - castle.y;
      const left_diff = castle.x + castle.width - player.x;
      const right_diff = player.x + player.width - castle.x;
  
      const min = Math.min(bottom_diff, top_diff, left_diff, right_diff);
      collided =  {
        bottom: bottom_diff == min,
        right: right_diff == min,
        left: left_diff == min,
        top: top_diff == min
        }
    }
    else
        { collided = {
            bottom: false,
            right: false,
            left: false,
            top: false}
        }
//console.log(collided)
return null;
}

//assign dpad actions to player actions
function clickDpadYellow(){
    gamerInput.action = "Up";
    direction = 4;
}
function clickDpadBlue(){
    gamerInput.action = "Left";
    direction = 2;
}
function clickDpadRed(){
    gamerInput.action = "Right";
    direction = 3;
}
function clickDpadGreen(){
    gamerInput.action = "Down";
    direction = 1;
}

//score function
function updateScore(){

    context.font = "20px Verdana";
    context.fillText("Score: " + score , 900 , 20);
}


// collision for hitting edge of screen
function canvascollision(){
    if (player.x > canvas.width - player.width){ // checks if player is hitting the right edge of the canvas
        collided.right = true;
    }
    if (player.x < 0 ){
        collided.left = true;
    }
    if (player.y > canvas.height - player.width){
        collided.bottom = true;
    }
    if (player.y < 0){
        collided.top = true;
    }
}


//draw healthbar
  function drawHealthbar() { // draws current health bar
  
    context.fillStyle = "#000000";
  context.clearRect(0, 0, 100,20);
  context.fillRect(0, 0, width, height);

  // Draw the fill
  context.fillStyle = "#0000FF";
  var fillVal = Math.min(Math.max(val / max, 0), 1);
  context.fillRect(0, 0, fillVal * width, height);
  }



 // if player collides with the health pot
function healthcollision(){
    if (player.x + player.width >= healthpot.x && player.x <= healthpot.x + healthpot.width &&
        player.y + player.height >= healthpot.y && player.y <= healthpot.y + healthpot.height)
    {
        randompos()
        val = val + 1;
        score = score + 1;
        localStorage.setItem("savedscore", score);
    }
}

function animateMoveDown(){

    animationtimer++;
    if (animationtimer > 6)
    {
        frame++;
        if (frame > 5){
            frame = 0;
        }
        animationtimer = 0;
    }
    row = 0;
    column = frame % 6;
}
function animateMoveLeft(){
    animationtimer++;
    if (animationtimer > 6)
    {
        frame++;
        if (frame > 5){
            frame = 0;
        }
        animationtimer = 0;
    }
    row = 1;
    column = frame % 6;
}
function animateMoveRight(){
    animationtimer++;
    if (animationtimer > 6)
    {
        frame++;
        if (frame > 5){
            frame = 0;
        }
        animationtimer = 0;
    }
    row = 2;
    column = frame % 6;
}
function animateMoveUp(){
    animationtimer++;
    if (animationtimer > 6)
    {
        frame++;
        if (frame > 5){
            frame = 0;
        }
        animationtimer = 0;
    }
    row = 3;
    column = frame % 6;
}

function animate(){

if (direction == 1){ // Down
animateMoveDown();
};
if (direction == 3){ // Right
    animateMoveRight();
};
if (direction == 2){  // Left
    animateMoveLeft();
};
    if (direction == 4){ // Up
    animateMoveUp();
};
    context.drawImage(player.spritesheet,
        column * player.width,
        row * player.height,
        player.width,
        player.height,
        player.x,
        player.y,
        137,
        137);
        //console.log("drawing char")
}


// on any gamer input of left up right or down will move the player.
function updatePlayerMovement(){
    if (gamerInput.action === "Left"){
        // console.log("left key pressed");
        if (collided.left != true){
           player.x -= speed; // Move Player Left
       }    
    } 
   else if (gamerInput.action === "Up"){
       // console.log("left key pressed");
       if (collided.top != true){
           player.y -= speed; // Move Player Up
       }       
   } 
   else if (gamerInput.action === "Right"){
       // console.log("left key pressed");
       if (collided.right != true){
           player.x += speed; // Move Player Right
       }
   } 
   else if (gamerInput.action === "Down") {
       //console.log("Move Down");
       if (collided.bottom != true){
           player.y += speed; // Move Player Down
       }
   } 
   else {(direction = 0)}
}
// random number generator for range
function GenerateRandomIntegerInRange(min,max)
{
    return Math.floor(Math.random()*(max - min + 1)) + min;
}

//assignt random position to potion spawning
function randompos(){
    let randomPotionPosX = GenerateRandomIntegerInRange(0, 700);
    let randomPotionPosY = GenerateRandomIntegerInRange(400, 700);
    healthpot.x = randomPotionPosX;
    healthpot.y = randomPotionPosY;
}
// nipplejs
var dynamic = nipplejs.create({
    color: 'grey',
    zone: document.querySelector(".joystickdiv"),
    position: {left: "300px"}, mode: "semi", catchDistance: 80
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       console.log("direction up");
       gamerInput = new GamerInput("Up");
       direction = 4;
    });
    nipple.on('dir:down', function (evt, data) {
        console.log("direction down");
        gamerInput = new GamerInput("Down");
        direction = 1;
     });
     nipple.on('dir:left', function (evt, data) {
        console.log("direction left");
        gamerInput = new GamerInput("Left");
        direction = 2;
     });
     nipple.on('dir:right', function (evt, data) {
        console.log("direction right");
        gamerInput = new GamerInput("Right");
        direction = 3;
     });
     nipple.on('end', function (evt, data) {
        console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
        direction = 0;
     });
});


function update() {
    collision();
    canvascollision();
    healthcollision();
    updatePlayerMovement();

    
    
    //console.log("Update");

    
    if (gamerInput.action === "q"){
        // console.log("q key pressed");
        player.height += 1;
        player.width += 1;
    }
    if (gamerInput.action === "w"){
        // console.log("w key pressed");
        player.height -= 1;
        player.width -= 1;
    }
    if (gamerInput.action === "e"){
        timer++;
        if (timer > 30){
        // console.log("e key pressed");
        document.getElementById("the_canvas").style.backgroundColor = "#" + Math.floor(Math.random()*16777215).toString(16);  // formula to get a random color and then apply it to the canvas background once is pressed
        timer = 0;
        console.log(document.getElementById("the_canvas").style.backgroundColor = Math.floor(Math.random()*16777215).toString(16)) // logs the # value of a random hex color
    }
}
}

function draw() {

   //clear canvas at start of code.
    context.clearRect(0, 0, canvas.width, canvas.height);
    animate();
    context.fillRect(500, 200, 400, 100);
    context.drawImage(castle.spritesheet, castle.x, castle.y, castle.width, castle.height);
    context.drawImage(healthpot.spritesheet, healthpot.x,healthpot.y, healthpot.width, healthpot.height);
    drawHealthbar();
    updateScore();
}


function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}
window.requestAnimationFrame(gameloop);
